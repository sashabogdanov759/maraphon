#include <iostream>
#include <iomanip>
#include "members_of_maraphone.h"
#include "file_reader.h"
#include "constans.h"
#include "filter.h"
using namespace std;
void output(member_maraphone* member_maraphone)
{
	cout << "***** �������� �������� *****\n\n";
	cout << member_maraphone->number << endl;
	//�������
	cout << member_maraphone->member.last_name << '\n';
	//���
	cout << member_maraphone->member.first_name << '\n';
	//��������
	cout << member_maraphone->member.middle_name << '\n';
	// ����� ����
	cout << setw(2) << setfill('0') << member_maraphone->start.hour << '-';
	// ����� �����
	cout << setw(2) << setfill('0') << member_maraphone->start.minutes << '-';
	// ����� ������
	cout << setw(2) << setfill('0') << member_maraphone->start.second;
	cout << '\n';
	/********** ����� ���� �������� **********/
	// ����� ����
	cout << "���� ������...: ";
	cout << setw(2) << setfill('0') << member_maraphone->finish.hour << '-';
	// ����� �����
	cout << setw(2) << setfill('0') << member_maraphone->finish.minutes << '-';
	// ����� ������
	cout << setw(2) << setfill('0') << member_maraphone->finish.second;
	cout << '\n';
	cout << member_maraphone->club << '\n';
}


int main()
{
	setlocale(LC_ALL, "Russian");
	member_maraphone* member[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("�����.txt", member, size);
		cout << "*****   *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(member[i]);
		}
		bool (*check_function)(member_maraphone*); // check_function -    ,    bool,
													//        book_subscription*
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_club_member; //       
			cout << "*****� ��������*****\n\n";
			break;
		case 2:
			check_function = check_member_by_result; //       
			cout << "*****��������� �������*****\n\n";
			break;
		default:
			throw "  ";
		}
		int new_size;
		member_maraphone** filtered = filter(member, size, check_function, new_size);
		for (int i = 0; i < new_size; i++)
		{
			output(filtered[i]);
		}
		delete[] filtered;
		for (int i = 0; i < size; i++)
		{
			delete member[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
}
