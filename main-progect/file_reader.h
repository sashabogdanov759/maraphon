#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "members_of_maraphone.h"

void read(const char* file_name, member_maraphone* array[], int& size);

#endif
